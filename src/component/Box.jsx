import React from 'react'

export default function Box(props) {
    const { value, onClick,id } = props
    return (
        <div id={id} className='box' onClick={onClick}>
            <h1>{value}</h1>
        </div>
    )
}
