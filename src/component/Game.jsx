import React from 'react'
import Box from './Box'

export default function Game() {
    let playOrNot = false
    const arrOfBoard = Array(9).fill(null)
    const [state, setState] = React.useState(arrOfBoard)
    const [turn, setTurn] = React.useState(true)

    const handleClick = (index) => {
        if (!playOrNot) {
            if (state[index] === null) {
                const copyOfarray = [...state]
                copyOfarray[index] = turn ? "X" : "0"
                setState(copyOfarray)
                setTurn(!turn)
            }
        }
    }
    const winnerArray = []
    function winOrNot() {
        const arr = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ]
        for (let value of arr) {
            const [a, b, c] = value
            if (state[a] != null && state[a] === state[b] && state[a] === state[c]) {
                winnerArray.push(a)
                winnerArray.push(b)
                winnerArray.push(c)
                return state[a]
            }
        }
        return false
    }
    const playAgain = () => {
        setState(arrOfBoard)
        if(winnerArray.length===3){
            document.getElementById(winnerArray[0]).classList.remove("green")
            document.getElementById(winnerArray[1]).classList.remove("green")
            document.getElementById(winnerArray[2]).classList.remove("green")
            for (let index = winnerArray.length - 1; index >= 0; index--) {
                winnerArray.pop();
            }
        }
    }
    const isWinner = winOrNot()
    let status
    let draw = 0;
    for (let value of state) {
        if (value != null) {
            draw++;
        }
    }
    if (draw == 9) {
        if (isWinner) {
            status = `Congratulation!! ${isWinner} won the match.`
            document.getElementById(winnerArray[0]).classList.add("green")
            document.getElementById(winnerArray[1]).classList.add("green")
            document.getElementById(winnerArray[2]).classList.add("green")
            playOrNot = true
        } else {
            status = "Match draw !!"
            playOrNot = true
        }
    }
    else if (!isWinner) {
        status = `Next turn is ${turn ? "X" : "0"}`
    }
    else {
        status = `Congratulation!! ${isWinner} won the match. `
        document.getElementById(winnerArray[0]).classList.add("green")
        document.getElementById(winnerArray[1]).classList.add("green")
        document.getElementById(winnerArray[2]).classList.add("green")
        playOrNot = true
    }
    return (
        <div className='game'>
            <div className="heading">
                <h1>Tic-Tac-Toe</h1>
            </div>
            <div className="status"><h1>{status}</h1></div>
            <div className="board">
                <div className="boardRow">
                    <Box id={0} onClick={() => handleClick(0)} value={state[0]} />
                    <Box id={1} onClick={() => handleClick(1)} value={state[1]} />
                    <Box id={2} onClick={() => handleClick(2)} value={state[2]} />
                </div>
                <div className="boardRow">
                    <Box id={3} onClick={() => handleClick(3)} value={state[3]} />
                    <Box id={4} onClick={() => handleClick(4)} value={state[4]} />
                    <Box id={5} onClick={() => handleClick(5)} value={state[5]} />
                </div>
                <div className="boardRow">
                    <Box id={6} onClick={() => handleClick(6)} value={state[6]} />
                    <Box id={7} onClick={() => handleClick(7)} value={state[7]} />
                    <Box id={8} onClick={() => handleClick(8)} value={state[8]} />
                </div>
            </div>
            <button onClick={playAgain} className="playAgain"><h1>Play Again</h1></button>
        </div>
    )
}